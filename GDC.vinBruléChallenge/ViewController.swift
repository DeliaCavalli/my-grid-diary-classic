//
//  ViewController.swift
//  GDC.vinBruléChallenge
//
//  Created by Delia Cavalli on 10/12/2019.
//  Copyright © 2019 Delia Cavalli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    // MARK: - VARIABLES
    
    var cellNumber = 8
    var collectionReflection : [String] = []
    var longPressGesture: UILongPressGestureRecognizer! = UILongPressGestureRecognizer()
    var cells: [UICollectionViewCell] = []
    var isLightMode : Bool = false
    
    // MARK: - OUTLETS AND BUTTONS
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBAction func deleteCellButton(_ sender: Any) {
    }
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBAction func addCellButton(_ sender: Any) {
        
        cellNumber = cellNumber+1
        let insertInIndexPath = IndexPath(item: collectionView.numberOfItems(inSection: 0), section: 0)
        collectionView.insertItems(at: [insertInIndexPath])
        
        if cellNumber > 8 {
            collectionView.scrollToItem(at: insertInIndexPath, at: .bottom, animated: true)
        }
    }
    
    @IBAction func magicWandButton(_ sender: Any) {
        
        collectionView.reloadData()
    }
    
        // drag and drop

        @objc func handleLongGesture(gesture: UILongPressGestureRecognizer){
        
        switch (gesture.state) {
        case .began:
            guard let selectedIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                break
            }
            collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view))
        case.ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
        
    }
    
    
         // MARK: - OVERRIDE FUNCTION
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // collection view positions and distances
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        layout.minimumLineSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width-15)/2, height: self.collectionView.frame.size.height/4)
        
        // vertical scroll hidden
        
        collectionView.showsVerticalScrollIndicator = false
        
        // drag and drop gesture recognizer
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        collectionView.addGestureRecognizer(longPressGesture)
        
        // inizialize PList
        
        MyPList()
        }
    }


        // MARK: - EXTENSION

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
        // collection view and cells functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        // colors, borders, opacity and dark mode
        
        collectionView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        collectionView.layer.opacity = 1
        cell.backgroundColor = .tertiarySystemGroupedBackground
        cell.layer.borderColor = #colorLiteral(red: 0.135856241, green: 0.1358864605, blue: 0.1358522475, alpha: 1).cgColor
        cell.layer.borderWidth = 1
        cell.layer.opacity = 2
        toolBar.barTintColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        dateLabel.textColor = #colorLiteral(red: 0.8882872462, green: 0.8814590573, blue: 0.8935094476, alpha: 1)
        overrideUserInterfaceStyle = .dark
        
        // random reflections on the added cells
        
        let label = cell.viewWithTag(100) as! UILabel
        
        if indexPath.row <= 7 {
            
            label.text = collectionReflection [indexPath.row]
        } else if indexPath.row >= 8 {
            
            label.text = collectionReflection[Int.random(in: 8...60)]
        }
        
        // magic randomization with wand button

        if cells.count > indexPath.row {
            
            label.text = collectionReflection[ Int.random(in: 0...60) ]
        } else {
            
            cells.append(cell)
        }
        
        // cells auto-resizing
        
        if cellNumber > 8  {
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: (self.collectionView.frame.size.width-15)/2, height: self.collectionView.frame.size.height/4.5)
        }
        
        return cell
        
    }
        // drag and drop functions
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = collectionReflection[sourceIndexPath.item]
        collectionReflection.remove(at: destinationIndexPath.item)
        collectionReflection.insert(item, at: destinationIndexPath.item)
    }
    
        // plist function
    
    func MyPList() {
        
        let archiveURL = Bundle.main.url(forResource: "Reflections", withExtension: ".plist")!
        
        let propertyListDecoder = PropertyListDecoder()
        if let retrievedStringData = try? Data(contentsOf: archiveURL),
            let decodedString = try? propertyListDecoder.decode([String].self, from: retrievedStringData) {
            
            collectionReflection = decodedString
        }
    }
    
    // BORDERCOLOR FOR CELL SELECTION
         
    // func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           //let cell = collectionView.cellForItem(at: indexPath)
           //cell?.layer.borderColor = UIColor.gray.cgColor
           //cell?.layer.borderWidth = 2
      // }
}

